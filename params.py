#params
import scipy.io as sio
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import LeavePGroupsOut as lpgo
import os

#data_params
os.chdir(os.path.dirname(__file__))
main_path=os.getcwd()

deep=True
if deep:
    data_path=os.path.join(main_path,'DFA_ROIs_all/')
else:
    data_path=os.path.join(main_path,'ROIs/')



save_path=os.path.join(main_path,'Classification_Results/')



#classification params
classification_mode='mf'
add_pe=False
stat=True
n_perms=1000
n_jobs=-1
clf_name='RF'

outer_cv= StratifiedKFold(10)
inner_cv=StratifiedKFold(5)
