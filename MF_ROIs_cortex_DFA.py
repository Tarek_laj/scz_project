    # -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 12:14:09 2017

@author: karim
"""
import os
import numpy as np
#from pylab import *
from itertools import product
import scipy.io as sio
from classification_utils import get_classifier,run_classification
from sklearn.model_selection import StratifiedKFold,cross_val_score,permutation_test_score
#from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from params import data_path,save_path,n_jobs,clf_name,test,outer_cv,inner_cv,n_perms,stat,classification_mode

def load_data(path=[],subject='Control',subj_list=[],session='open',bandes=[]):
    data=[]
    for sbj in subj_list:
        bd_pt_data=[]
        for bd in bandes:
            xp=np.load(os.join(path,'{sb}_{sb_nb}_DFA_{bd}_{sess}_ROIs.npy'.format(sb=subject,
                                                                                   sb_nb=sbj,
                                                                                   bd=bd,
                                                                                   sess=session)))

            bd_pt_data=np.hstack(bd_pt_data,xp) if bd_pt_data.size else bd_pt_data
        print(bd_pt_data.shape)
        data=np.vstack(data,bd_pt_data) if patient_data.size else bd_pt_data

    return data
moy=1
test=False
print('Params info : \n classification mode = {cl} \n Test mode : {test}'.format(cl=classification_mode,
                                                                                 test=str(test)))
print('Classifier: {clf} \n stat: {sts} with n_perms={n} '.format(clf=clf_name,
                                                                  sts=str(stat),
                                                                  n=str(n_perms)))

nb_rep=1000
Features_importance=True
permutation_test=False

patient_list = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','19','20','21','22','23','24','25','26']
control_list = ['01','03','05','06','07','08','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28']
sessions=['closed','open']
bandes=['delta','theta','alpha','beta','gamma']

for n in range(nb_rep):
    for sess in sessions:

        save_path_f=os.path.join(save_path,'{}/MultiFeatures/'.format(sess))
        if not os.path.exists(save_path_f):
            os.mkdir(save_path_f)


        patient_data=load_data(path=data_path,subject='Patient',subj_list=patient_list,session=sess,bandes=bandes)

        print(patient_data.shape)
        1/0


        #     x=np.vstack((Dr_data,nDr_data))
        #     print(x.shape)
        #     y=np.concatenate([np.ones(Dr_data.shape[0]),np.zeros(nDr_data.shape[0])],axis=0)
        #     clf=get_classifier(clf_name='RF', inner_cv=inner_cv)
        #
        #     if Features_importance:
        #         clf.fit(x, y)
        #         feat_importances =clf.best_estimator_.feature_importances_
        #         print(feat_importances)
        #     scores = cross_val_score(clf, X=x, y=y, cv=outer_cv)
        #     print(np.mean(scores))
        #     # feat_import.append(feat_importances)
        #     # DA.append(np.mean(scores))
        #     #
        #     save_dict={'DA':np.mean(scores),'Feat_import':feat_importances}
        #
        #     if permutation_test:
        #
        #         test_score, permutation_score, pvalue = permutation_test_score(
        #                                                 clf, x, y,
        #                                                 scoring="accuracy",
        #                                                 cv=outer_cv,
        #                                                 n_permutations=n_perms,
        #                                                 n_jobs=n_jobs)
        #         print("Test accuracy = %0.4f "%(test_score))
        #         save_dict.update({'DA_perm':permutation_score,'DA_test_perm':test_score,'pval':pvalue})
        #     print('saving results in :', save_file)
        # sio.savemat(save_file,save_dict)
