import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import os
import matplotlib.colors as mcolors
import pandas as pd
import seaborn as sns


def define_color(color='blue'):
    clist = [(0, color), (0.125, color), (1, color)]    #,(0.5, "orange"),(0.75, "orange"),(1, "orange")]
    rvb=mcolors.LinearSegmentedColormap.from_list("", clist)
    return rvb
session='closed'
deep=True
#load data

data_path='/home/karim/scz_project/Classification_Results/{sess}/MultiFeatures/'.format(sess=session)
if deep:
    feat_file='MF_deep_ROIs_RF_rep{}.mat'
else:
    feat_file='MF_cotrex_ROIs_RF_optm_rep{}.mat'
DA,feat_imp=np.array([]),np.array([])
for n in range(100):
    ck=sio.loadmat(os.path.join(data_path,feat_file.format(str(n))))
    feat_imp=np.vstack((feat_imp,ck['Feat_import'])) if feat_imp.size else ck['Feat_import']
    DA=np.vstack((DA,ck['DA'])) if DA.size else ck['DA']

mean_DA=np.mean(DA,axis=0)
mean_feat_imp=np.mean(feat_imp,axis=0)

#160 roi chaque bande
# 150 cortex +10 deep
delta,theta,alpha,beta,gamma=np.split(mean_feat_imp,5)
ncortext=150


# data_dict={'delta_cortex':delta_cortex,'delta_deep':delta_deep,
#            'theta_cortex':theta_cortex,'theta_deep':theta_deep,
#            'alpha_cortex':alpha_cortex,'alpha_deep':alpha_deep,
#            'beta_cortex':beta_cortex,'beta_deep':beta_deep,
#            'gamma_cortex':gamma_cortex,'gamma_deep':gamma_deep}
seaborn=False
if seaborn:
    bd=['Delta']*160+['Theta']*160+['Alpha']*160+['Beta']*160+['Gamma']*160
    bd_ALL=['Delta_cortex']*ncortext+['Delta_deep']*10+['Theta_cortex']*ncortext+['Theta_deep']*10+\
            ['Alpha_cortex']*ncortext+['Alpha_deep']*10+['Beta_cortex']*ncortext+['Beta_deep']*10+\
            ['Gamma_cortex']*ncortext+['Gamma_deep']*10
    print(len(bd_ALL))
    ff=np.hstack((delta,theta,alpha,beta,gamma)).T
    data_dict={'ROI': np.arange(160*5),
               'Bande':bd_ALL,
               'Feature importance':ff
              }
    df=pd.DataFrame(data_dict)
    ax = sns.barplot(x="ROI", y="Feature importance",hue='Bande', data=df,palette="hsv")
    #plt.show()
else:
    delta_cortex=np.sort(delta[:ncortext])[::-1]
    delta_deep=np.sort(delta[ncortext:])[::-1]
    theta_cortex=np.sort(theta[:ncortext])[::-1]
    theta_deep=np.sort(theta[ncortext:])[::-1]
    alpha_cortex=np.sort(alpha[:ncortext])[::-1]
    alpha_deep=np.sort(alpha[ncortext:])[::-1]
    beta_cortex=np.sort(beta[:ncortext])[::-1]
    beta_deep=np.sort(beta[ncortext:])[::-1]
    gamma_cortex=np.sort(gamma[:ncortext])[::-1]
    gamma_deep=np.sort(gamma[ncortext:])[::-1]

    r_crx=np.arange(15,165)
    r_dp=np.arange(10)
    w=2
    Bandes=['Delta cortex','Delta deep',
            'Theta cortex','Theta deep',
            'Alpha cortex','Alpha deep',
            'Beta cortex','Beta deep',
            'Gamma cortex','Gamma deep']
    # rvb_delta=define_color(color='red')
    # rvb_dp_delta=define_color(color='orange')
    plt.figure(figsize=(25,10))
    plt.bar(r_crx, delta_cortex, color='gray', edgecolor='white', width=w,label='Delta cortex')
    plt.bar(r_dp, delta_deep, color='black', edgecolor='white', width=w,label='Delta deep')

    # rvb_theta=define_color(color='darkgreen')
    # rvb_dp_theta=define_color(color='mediumseagreen')
    plt.bar(r_crx+160, theta_cortex, color='lime', edgecolor='white',  width=w,label='Theta cortex')
    plt.bar(r_dp+160, theta_deep, color='darkgreen', edgecolor='white', width=w,label='Theta deep')

    # rvb_alpha=define_color(color='royalblue')
    # rvb_dp_alpha=define_color(color='navy')
    plt.bar(r_crx+320, alpha_cortex, color='skyblue', edgecolor='white', width=w,label='Alpha cortex')
    plt.bar(r_dp+320, alpha_deep, color='navy', edgecolor='white', width=w,label='Alpha deep')

    plt.bar(r_crx+3*160, beta_cortex, color='fuchsia', edgecolor='white', width=w,label='Beta cortex')
    plt.bar(r_dp+3*160, beta_deep, color='darkmagenta', edgecolor='white', width=w,label='Beta deep')

    plt.bar(r_crx+4*160, gamma_cortex, color='orange', edgecolor='white',width=w, label='Gamma cortex')
    plt.bar(r_dp+4*160, gamma_deep, color='red', edgecolor='white', width=w,label='Gamma deep')
    plt.ylabel('Feature importance',fontsize=20)
    #plt.xlabel('ROIs accross frequecy bands')
    plt.xticks([80,240,400,560 ,720], ('Delta ROIs', 'Theta ROIs', 'Alpha ROIs', 'Beta ROIs', 'Gamma ROIs'),fontsize=20)
    #plt.xticks([])
    plt.legend(Bandes,fontsize=16)
    #plt.show()

    plt.savefig('Feat_importance.tiff',dpi=600)

# cortex_feat=np.sort(mean_feat_imp[:150])[::-1]
# deep_feat=np.sort(mean_feat_imp[150:])[::-1]
# print(deep_feat.shape)
# x=np.arange(cortex_feat.shape[0])
# x_dp=np.arange(deep_feat.shape[0])
# plt.bar(x,cortex_feat,color=rvb(x/cortex_feat.shape[0]))
# plt.bar(x_dp,deep_feat,color=rvb_dp(x_dp/deep_feat.shape[0]))
# plt.show()
